const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/",(req,res)=>{
	res.send("Hello World");
});

app.get("/hello",(req,res)=>{
	res.send("Hello");
});

app.post("/hello",(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users = [];
app.post("/signup",(req,res)=>{
	console.log(req.body);
	if(req.body.firstName !== "" && req.body.lastName !== ""){
		users.push(req.body)
		res.send(`${req.body.firstName} signed up successfully.`)
		console.log(users);
	} else{
		res.send(`firstName and lastName should be filled with info.`)
	}
});

app.put("/change-lastName",(req,res)=>{
	let message;

	for(let i = 0; i<users.length; i++){
		if(req.body.firstName == users[i].firstName){
			users[i].lastName = req.body.lastName;

			message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`

			break;
		} else{
			message = "User does not exist";
		}
	}
	res.send(message);
});



// ACTIVITY SECTION
app.get("/home",(req,res)=>{
	res.send("Welcome to the home page");
});

app.get("/users",(req,res)=>{
	res.send(users);
});

app.delete("/delete-user",(req,res)=>{
	let message
	for(let i = 0; i<users.length; i++){
		if(req.body.username == users[i].username){
			users.splice(i);

			message = `User ${req.body.username} has been deleted.`

			break;
		} else{
			message = "User does not exist";
		}
	}
	res.send(message);
});


app.listen(port,()=> console.log(`Server running at port ${port}`));